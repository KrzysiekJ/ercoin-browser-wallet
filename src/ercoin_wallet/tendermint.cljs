(ns ercoin-wallet.tendermint
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
   [alphabase.hex :as hex]
   [cljs.core.async :refer [<! promise-chan]]
   [cljs-http.client :as http]
   [clojure.string :refer [split]]
   [goog.crypt.base64 :as b64]
  ))

(enable-console-print!)

(defn request
  ([endpoint method]
   (request endpoint method {}))

  ([endpoint method params]
   (go
     (let [url (str endpoint "/" method)
           resp-chan (promise-chan)
           response (<! (http/get url
                                  {:query-params params
                                   :server-port 46657}))]
       (assert (= 200 (:status response)))
       (assert (:success response))
       (println response)
       (get-in response [:body :result])))))

(defn query [endpoint path data]
  (go
    (let [params {:path (str "\"" path "\"")
                  :data (str "0x" (hex/encode (clj->js data)))}
          result (<! (request endpoint "abci_query" params))
          value (get-in result [:response :value] "")]
      (assert value)
      (js->clj (b64/decodeStringToByteArray value))
      )))

(defn height [endpoint]
  (go
    (let [resp-chan (promise-chan)
          result (<! (request endpoint "abci_info"))]
      (js/parseInt (get-in result [:response :last_block_height])))))
