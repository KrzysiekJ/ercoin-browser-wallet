(ns ercoin-wallet.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
   [alandipert.storage-atom :refer [local-storage]]
   [alphabase.hex :as hex]
   [cljs.core.async :refer [<!]]
   [reagent.core :as r]
   [cljsjs.nacl-fast]
   [ercoin-wallet.account :as a]
   [ercoin-wallet.binary :as binary]
   [ercoin-wallet.converter :as converter]
   [ercoin-wallet.tendermint :as tendermint]
   [ercoin-wallet.timestamp :as timestamp]
   [ercoin-wallet.tx :as tx]
   [ercoin-wallet.uint :as uint]
   [ercoin-wallet.units :as units]))

;; State

(defonce accounts
  (local-storage (r/atom {}) :accounts))
(defonce rpc-endpoint
  (local-storage (r/atom "http://localhost:26657") :rpc-endpoint))
(defonce binary-encoding
  (local-storage (r/atom :base58) :binary-encoding))
(defonce unit
  (local-storage (r/atom :ercoin) :unit))

;; Tendermint communication

(defn fetch-account [address]
  (go
    (let [value (<! (tendermint/query
                     @rpc-endpoint
                     "account"
                     address))]
      (a/deserialize address value))))

(defn update-account! [{:keys [::a/address] :as data}]
  (swap! accounts
         #(assoc % address (merge (get % address) data))))

(defn put-account! [{:keys [::a/address] :as account}]
  (swap! accounts #(assoc % address account)))

(defn refresh-account! [{:keys [::a/address] :as account}]
  (go
    (let [fresh-account (<! (fetch-account address))]
      (put-account! (merge account fresh-account)))))

(defn refresh-accounts! []
  (doseq [account (vals @accounts)]
    (refresh-account! account)))

;; Converters

(defn binary-decode [string]
  (converter/decode (binary/get-converter @binary-encoding) string))

(defn binary-encode [vect]
  (converter/encode (binary/get-converter @binary-encoding) vect))

(defn unit-encode [value]
  (converter/encode (units/get-unit @unit) value))

(defn unit-decode [value-str]
  (converter/decode (units/get-unit @unit) value-str))

;; Views

(defn hidden-key [key]
  [:input {:type :button
           :value "Show"
           :on-click #(js/alert (binary-encode key))}])

(defn account-row [account]
  (let [editing (r/atom false)]
    (fn [{:keys [::a/label
                 ::a/address
                 ::a/private-key
                 ::a/balance
                 ::a/valid-until
                 ::a/locked-until
                 ::a/validator-address] :as account}]
      [:tr
       [:td
        {:on-double-click #(reset! editing true)
         :on-key-down #(case (.-which %)
                         13 (reset! editing false)
                         27 (reset! editing false)
                         nil)}
        (if @editing
          [:input {:type :text
                   :value label
                   :on-change (fn [e] (swap! accounts #(assoc-in
                                                        %
                                                        [address ::a/label]
                                                        (-> e .-target .-value))))}]
          label)]
       [:td
        [:code (binary-encode address)]]
       [:td
        (when private-key
          [hidden-key private-key])]
       [:td (unit-encode balance)]
       [:td (timestamp/format valid-until)]
       [:td (timestamp/format locked-until)]
       [:td
        [:code (binary-encode validator-address)]]
       [:td
        [:input {:type :button
                 :value "Remove"
                 :on-click (fn [_] (when (js/confirm "Are you sure you want to remove the account?")
                                     (swap! accounts #(dissoc % address))))}]
        [:input {:type :button
                 :value "Refresh"
                 :on-click #(refresh-account! account)}]]
       ])))

(defn accounts-table []
  [:table
   [:thead
    [:tr
     [:th "Label"]
     [:th "Address"]
     [:th "Private key"]
     [:th "Balance"]
     [:th "Valid until"]
     [:th "Locked until"]
     [:th "Validator address"]
     [:th "Operations"]
     ]]
   [:tbody
    (for [account (vals @accounts)]
      ^{:key (::a/address account)} [account-row account])]])

(defn rpc-endpoint-field []
  [:p
   "RPC endpoint: "
   [:input {:type :text
            :value @rpc-endpoint
            :on-change #(reset! rpc-endpoint (-> % .-target .-value))}]])

(defn binary-encoding-field []
  [:p
   "Binary encoding: "
   [:select
    {:defaultValue @binary-encoding
     :on-change #(reset! binary-encoding (-> % .-target .-value keyword))}
    (for [{key :key name :name} binary/all-converters]
       ^{:key key}
      [:option
       {:value key}
       name])]])

(defn unit-field []
  [:p
   "Unit: "
   [:select
    {:defaultValue @unit
     :on-change #(reset! unit (-> % .-target .-value keyword))}
    (for [{:keys [:name :key]} units/all]
      ^{:key key}
      [:option
       {:value key}
       name])]])

(defn accounts-refresh-button []
  [:p
   [:input {:type :button
            :value "Refresh all"
            :on-click #(refresh-accounts!)}]])

(defn account-select [label account-atom]
  [:p
   [:label label]
   [:select
    {:on-change #(reset! account-atom
                         (get @accounts (converter/decode binary/raw (-> % .-target .-value))))}
    [:option "———"]
    (for [account (vals @accounts)]
      (let [address (::a/address account)]
        ^{:key address}
        [:option
         {:value address}
         (::a/label account)]))]])

(defn amount-field
  ([amount-atom] (amount-field amount-atom "Amount"))
  ([amount-atom label]
   (let [{:keys [:resolution :symbol]} (units/get-unit @unit)]
     [:p
      [:label label]
      [:input {:type :number
               :value (unit-encode @amount-atom)
               :min 0
               :step (if (= 0 resolution)
                       1
                       (apply str (concat "0." (repeat (- resolution 1) 0) "1")))
               :on-change #(reset! amount-atom (-> % .-target .-value (unit-decode)))}]
      " " symbol])))

(defn broadcast-tx [tx]
  (go
    (let [result (<! (tendermint/request
                      @rpc-endpoint
                      "broadcast_tx_commit"
                      {:tx (str "0x" (hex/encode (clj->js tx)))}))]
      (js/alert result))))

(defn timestamp-field
  ([timestamp-atom] (timestamp-field timestamp-atom "Timestamp"))
  ([timestamp-atom label] (timestamp-field
                          timestamp-atom
                          label
                          [["Now" timestamp/now]]))
  ([timestamp-atom label predefined-values]
   (let [change-atom (r/atom 1)
         interval-atom (r/atom (* 24 3600))]
     (fn []
       [:p
        [:label label]
        " "
        [:span (timestamp/format @timestamp-atom)]
        (for [[value-label value-func] predefined-values]
          ^{:key value-label}
          [:input {:type :button
                   :value value-label
                   :on-click #(reset! timestamp-atom (value-func))}])
        [:input {:type :number
                 :value @change-atom
                 :on-change #(reset! change-atom (-> % .-target .-value))}]
        [:select {:defaultValue @interval-atom
                  :on-change #(reset! interval-atom (-> % .-target .-value js/parseInt))}
         (for [[name interval] [["second" 1]
                                ["minute" 60]
                                ["hour" 3600]
                                ["day" (* 24 3600)]
                                ["30 days" (* 30 24 3600)]
                                ["365 days" (* 365 24 3600)]]]
           ^{:key interval}
           [:option {:value interval}
            name])]
        [:input {:type :button
                 :value "+"
                 :on-click (fn [e] (swap! timestamp-atom #(+ % (* @change-atom @interval-atom))))}]
        [:input {:type :button
                 :value "−"
                 :on-click (fn [e] (swap! timestamp-atom #(- % (* @change-atom @interval-atom))))}]
        ]))))

(defn generated-tx [tx-atom]
  (when @tx-atom
    [:div
     [:p
      [:label "Generated transaction: "]
      [:code (binary-encode @tx-atom)]]
     [:p
      [:input {:type :button
               :value "Broadcast"
               :on-click #(broadcast-tx @tx-atom)}]]]))

(defn transfer-tx-form []
  (let [from (r/atom nil)
        to (r/atom nil)
        amount (r/atom nil)
        message (r/atom "")
        timestamp (r/atom nil)
        tx (r/atom nil)]
    (fn []
      [:div
       (account-select "From" from)
       (account-select "To" to)
       (amount-field amount)
       [:p
        [:label "Message"]
        [:input {:type :text
                 :value @message
                 :on-change #(reset! message (-> % .-target .-value))}]]
       [timestamp-field timestamp]
       [:p
        [:input {:type :button
                 :on-click #(reset! tx (tx/transfer-tx @timestamp @from @to @amount @message))
                 :value "Generate"}]]
       [generated-tx tx]
       ])))

(defn account-tx-form []
  (let [valid-until (r/atom nil)
        from (r/atom nil)
        to (r/atom nil)
        tx (r/atom nil)]
    (fn []
      [:div
       [timestamp-field valid-until "Valid until" [["Current" (fn []
                                                                (or (::a/valid-until @from)
                                                                    (timestamp/now)))]
                                                   ["Now" timestamp/now]]]
       (account-select "From" from)
       (account-select "To" to)
       [:p
        [:input {:type :button
                 :on-click #(reset! tx (tx/account-tx @valid-until @from @to))
                 :value "Generate"}]]
       [generated-tx tx]])))

(defn lock-tx-form []
  (let [locked-until (r/atom nil)
        account (r/atom nil)
        validator (r/atom nil)
        tx (r/atom nil)]
    (fn []
      [:div
       [timestamp-field locked-until "Locked until" [["Current" (fn []
                                                                  (or (::a/locked-until @account)
                                                                      (timestamp/now)))]
                                                     ["Now" timestamp/now]]]
       (account-select "Account" account)
       (account-select "Validator" validator)
       [:p
        [:input {:type :button
                 :on-click #(reset! tx (tx/lock-tx @locked-until @account @validator))
                 :value "Generate"}]]
       [generated-tx tx]])))

(defn vote-tx-form []
  (let [validator (r/atom nil)
        timestamp (r/atom nil)
        per-tx (r/atom nil)
        per-256b (r/atom nil)
        per-account-day (r/atom nil)
        protocol (r/atom 1)
        tx (r/atom nil)]
    (fn []
      [:div
       [account-select "Validator" validator]
       [amount-field per-tx "Fee per tx"]
       [amount-field per-256b "Fee per 256 bytes"]
       [amount-field per-account-day "Fee per account/day"]
       [:p
        [:label "Protocol"]
        [:input {:type :number
                 :value @protocol
                 :readOnly true}]]
       [timestamp-field timestamp]
       [:p
        [:input {:type :button
                 :on-click #(reset! tx (tx/vote-tx @timestamp @validator @per-tx @per-256b @per-account-day @protocol))
                 :value "Generate"}]]
       [generated-tx tx]
       ])))

(defn genesis-tx-form []
  (let [from (r/atom nil)
        locked (r/atom false)
        locked-for (r/atom nil)
        validator (r/atom nil)
        tx (r/atom nil)]
    (fn []
      [:div
       (account-select "Address" from)
       [:p
        [:label "Locked"]
        [:input {:type :checkbox
                 :on-change #(do
                               (reset! locked (-> % .-target .-checked))
                               (reset! locked-for nil))}]]
       (when @locked
         [:div
          [:p
           [:label "Locked for"]
           [:input {:type :number
                    :min 1
                    :max (- (js/Math.pow 2 24) 1)
                    :on-change #(reset! locked-for (-> % .-target .-value js/parseInt))}]]
          (account-select "Validator" validator)])
       [:p
        [:input {:type :button
                 :on-click #(reset! tx (tx/genesis-tx @from @locked-for @validator))
                 :value "Generate"}]]
       (when @tx
         [:div
          [:p
           [:label "Generated transaction: "]
           [:code (hex/encode (clj->js @tx))]]])
       ])))

(defn transaction-form []
  (let [selected-type (r/atom :transfer)
        types (array-map :transfer transfer-tx-form
                         :account account-tx-form
                         :lock lock-tx-form
                         :vote vote-tx-form
                         :genesis genesis-tx-form)]
    (fn []
      [:div
       [:select
        {:on-change #(reset! selected-type (-> % .-target .-value keyword))}
        (for [[type _] types]
          ^{:key type}
          [:option {:value type}
           type])]
       [(get types @selected-type)]
       ])))

(defn new-account-form []
  (let [visible (r/atom (empty? @accounts))
        account (r/atom {})]
    (fn []
      (if @visible
        [:div
         [:h3 "Add account"]
         [:p
          [:label "Label"]
          [:input {:type :text
                   :value (::a/label @account)
                   :on-change (fn [e] (swap! account #(assoc % ::a/label (-> e .-target .-value))))}]]
         [:p
          [:label "Public key"]
          [:input {:type :text
                   :value (binary-encode (::a/address @account))
                   :on-change (fn [e] (swap! account #(assoc % ::a/address (->
                                                                               e
                                                                               .-target
                                                                               .-value
                                                                               binary-decode))))}]]
         [:p
          [:label "Private key"]
          [:input {:type :text
                   :value (binary-encode (::a/private-key @account))
                   :on-change (fn [e] (swap! account #(assoc % ::a/private-key (->
                                                                                e
                                                                                .-target
                                                                                .-value
                                                                                binary-decode))))}]]
         [:input {:type :button
                  :value "Abort"
                  :on-click #(do
                               (reset! visible false)
                               (reset! account {}))}]
         [:input {:type :button
                  :value "Add"
                  :on-click (fn [_] (do
                                      (put-account! (a/add-keypair @account))
                                      (reset! account {})))}]]
        [:input {:type :button
                 :value "Add account"
                 :on-click #(reset! visible true)}]))))

(defn home-page []
  [:div
   [:section [:h2 "Configuration"]
    [rpc-endpoint-field]
    [binary-encoding-field]
    [unit-field]
    ]
   [:section [:h2 "Accounts"]
    [accounts-refresh-button]
    [:p [:kbd "Double click"] " label to edit, " [:kbd "Enter"] " or " [:kbd "Esc"] " to finish editing."]
    [accounts-table]
    [new-account-form]]
   [:section [:h2 "Create transaction"]
    [transaction-form]
    ]])

;; Initialize app

(defn mount-root []
  (r/render [home-page] (.getElementById js/document "app")))

(defn init! []
  (mount-root))
