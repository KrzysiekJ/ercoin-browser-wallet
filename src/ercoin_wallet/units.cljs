(ns ercoin-wallet.units
  (:require
   [clojure.string :as string]
   [ercoin-wallet.converter :refer [Converter]]
   ))

(defrecord Unit [name key symbol resolution]
  Converter
  (encode [this value]
    (let [divider (js/Math.pow 10 resolution)
          integer-part (quot value divider)
          fractional-part (rem value divider)]
      (if (> fractional-part 0)
        (let [fractional-str-raw (str fractional-part)]
          (str
           integer-part
           "."
           (apply str (repeat (- resolution (count fractional-str-raw)) 0))
           (string/replace fractional-str-raw #"0+$" "")))
        (str integer-part))))
  (decode [this value-str]
    (let [[integer-str fractional-str] (string/split value-str ".")
          integer-part (* (js/parseInt integer-str) (js/Math.pow 10 resolution))]
      (if fractional-str
        (+ integer-part
           (* (js/parseInt fractional-str) (js/Math.pow 10 (- resolution (count fractional-str)))))
        integer-part))))

(def ercoin (Unit. "ercoin" :ercoin "ERN" 9))
(def deciercoin (Unit. "deciercoin" :deciercoin "dERN" 8))
(def centiercoin (Unit. "centiercoin" :centiercoin "cERN" 7))
(def miliercoin (Unit. "miliercoin" :miliercoin "mERN" 6))
(def microercoin (Unit. "microercoin" :microercoin "µERN" 3))
(def nanoercoin (Unit. "nanoercoin" :nanoercoin "nERN" 0))
(def kiloercoin (Unit. "kiloercoin" :kiloercoin "kERN" 12))
(def megaercoin (Unit. "megaercoin" :megaercoin "MERN" 15))

(def all (list
          ercoin
          deciercoin
          centiercoin
          miliercoin
          microercoin
          nanoercoin
          kiloercoin
          megaercoin
         ))
(def all-map
  (into {} (map (fn [unit] [(:key unit) unit]) all)))

(defn get-unit [key]
  (get all-map key))
