(ns ercoin-wallet.tx
  (:require
   [cljsjs.nacl-fast]
   [ercoin-wallet.account :as a]
   [ercoin-wallet.uint :as uint]
  ))

(defn sign-detached [binary account]
  (-> (js/nacl.sign.detached (js/Uint8Array.from (clj->js binary))
                             (js/Uint8Array.from (clj->js (::a/private-key account))))
      js/Array.from
      js->clj))

(defn sign [binary account]
  (vec (concat binary
               (sign-detached binary account))))

(defn transfer-tx [timestamp from to amount message]
  (let [message-bytes (uint/str->utf8 message)
        to-sign (concat
                 [0]
                 (uint/uint->vec timestamp 4)
                 (::a/address from)
                 (::a/address to)
                 (uint/uint->vec amount 8)
                 [(count message-bytes)]
                 message-bytes)]
    (sign to-sign from)))

(defn account-tx [valid-until from to]
  (let [to-sign (concat
                 [1]
                 (uint/uint->vec valid-until 4)
                 (::a/address from)
                 (::a/address to))]
    (sign to-sign from)))

(defn lock-tx [locked-until account validator]
  (let [to-sign (concat
                 [2]
                 (uint/uint->vec locked-until 4)
                 (::a/address account)
                 (::a/address validator))]
    (sign to-sign account)))

(defn vote-tx [timestamp validator per-tx per-256b per-account-day protocol]
  (-> (concat
       [3]
       (uint/uint->vec timestamp 4)
       (::a/address validator)
       (uint/uint->vec per-tx 8)
       (uint/uint->vec per-256b 8)
       (uint/uint->vec per-account-day 8)
       [protocol])
      (sign validator)))

(defn genesis-tx [from locked-until validator]
  (let [locked-until-vec (drop-while #(= 0 %) (uint/uint->vec (or locked-until 0) 3))
        to-sign (concat
                 (uint/str->utf8 "Ercoin ")
                 (::a/address from)
                 [(count locked-until-vec)]
                 (if (> locked-until 0)
                   (::a/address validator)
                   [])
                 locked-until-vec)
        signed-msg (sign to-sign from)]
    (concat [106 76 (count signed-msg)]
            signed-msg)))
