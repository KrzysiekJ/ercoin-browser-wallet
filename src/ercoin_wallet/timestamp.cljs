(ns ercoin-wallet.timestamp)

(defn now []
  (quot (js/Date.now) 1000))

(defn format [timestamp]
  (if timestamp
    (.toISOString (js/Date. (* timestamp 1000)))
    "none"))
