# In-browser Ercoin wallet

This is a simple in-browser wallet for [Ercoin](http://ercoin.tech). Written in [ClojureScript](https://clojurescript.org) and [Reagent](https://github.com/reagent-project/reagent).

## Prerequesites

You need [Leiningen](https://leiningen.org) installed.

## Compilation

To compile the wallet into `public/` directory, run:

`lein package`

## Development mode

To start the Figwheel compiler, navigate to the project folder and run the following command in the terminal:

```
lein figwheel
```

Figwheel will automatically push ClojureScript and CSS changes to the browser.
Once Figwheel starts up, it will open the `public/index.html` page in the browser.

## License

This software is licensed under under [the Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0) (the “License”); you may not use this software except in compliance with the License. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License.
